package main.java;

import java.util.Scanner;

public class BlocoTres {

    public static void main(String[] args) {

        //ExOne();
        //ExTwo();
        //ExThree();
        //ExFourA();
        //ExFourB();
        //ExFourC();
        //ExFourD();
        //ExFourE();
        //ExFiveA();
        //ExFiveB();
        //ExFiveC();
        //ExFiveD();
        //ExFiveE();
        //ExFiveF();
        //ExFiveG();
        //ExFiveH();
        //ExSixA();
        //ExEight();
        //ExNine();
        //ExEleven();
        //ExTwelve();
        //ExThirteen();
        //ExFourteen();
        //ExFifteen();
        //ExSixteen();
        //ExSeventeen();
        ExEighteen();
        //ExNineteen();
        //ExTwenty();
    }


    public static void ExOne() {

        // Função para
        // Estrutura de dados
        int res, num;
        res = 1;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        num = ler.nextInt();

        // Processamento

        res = ExOneGetRes(res, num);

        // Escritura de dados
        System.out.println("O resultado é: " + res);
    }

    public static int ExOneGetRes(int res, int num) {

        for (int x = num; x > 1; x--) {
            res = res * x;
        }
        return res;
    }


    public static void ExTwo() {

        // Função para ler notas de N alunos e declarar a percentagem de positivas e média das negativas
        // Estrutura de dados
        int numberStudents, passing = 0, failing = 0;
        double percentagePassing, averageFailing, totalFailing = 0, grade;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de alunos");
        numberStudents = ler.nextInt();

        // Processamento

        for (int x = numberStudents; x > 0; x--) {

            System.out.println("Introduza a nota do aluno");
            grade = ler.nextDouble();

            if (grade >= 9.5) {
                passing++;
            } else {
                totalFailing = totalFailing + grade;
                failing++;
            }
        }

        percentagePassing = (passing / (double) numberStudents) * 100;
        averageFailing = totalFailing / failing;

        // Escritura de dados
        System.out.println("Notas Positivas: " + percentagePassing + "% . Média das negativas: " + String.format("%.2f", averageFailing));

    }


    public static void ExThree() {

        // Função para ler números positivos até um negativo e percentagem de pares e média de impares
        // Estrutura de dados
        int number = 0, evens = 0, odds = 0;
        double percentageEvens, averageOdds, totalOdds = 0, totalNumbers = 0;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        // Processamento

        while (number >= 0) {

            System.out.println("Introduza um número");
            number = ler.nextInt();

            if (number > 0) {

                totalNumbers++;

                if ((number % 2) == 0) {
                    evens++;
                } else {
                    totalOdds = totalOdds + number;
                    odds++;
                }
            }
        }

        percentageEvens = (evens / totalNumbers) * 100;
        averageOdds = totalOdds / odds;

        // Escritura de dados
        System.out.println("Números pares: " + percentageEvens + "% . Média dos impares: " + String.format("%.2f", averageOdds));

    }


    public static void ExFourA() {

        // Função para encontrar múltiplos de 3 num intervalo de números dado
        // Estrutura de dados
        int minimum, maximum, numberMultiples;
        final int MULTIPLE = 3;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();

        // Processamento

        numberMultiples = ExFourAGetNumberMultiples(minimum, maximum, MULTIPLE);

        // Escritura de dados
        System.out.println("Número de múltiplos de 3 no intervalo dado: " + numberMultiples);
    }

    public static int ExFourAGetNumberMultiples(int minimum, int maximum, int MULTIPLE) {
        int numberMultiples = 0;

        for (int x = minimum; x <= maximum; x++) {
            if (x != 0) {
                if ((x % MULTIPLE) == 0) {
                    numberMultiples++;
                }
            }
        }

        return numberMultiples;
    }


    public static void ExFourB() {

        // Função para encontrar múltiplos de um número dado num intervalo de números dado
        // Estrutura de dados
        int minimum, maximum, numberMultiples, multiple;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();
        System.out.println("Introduza o número");
        multiple = ler.nextInt();

        // Processamento

        numberMultiples = ExFourBGetNumberMultiples(minimum, maximum, multiple);

        // Escritura de dados
        System.out.println("Número de múltiplos de " + multiple + " no intervalo dado: " + numberMultiples);
    }

    public static int ExFourBGetNumberMultiples(int minimum, int maximum, int multiple) {
        int numberMultiples = 0;

        for (int x = minimum; x <= maximum; x++) {
            if (x != 0) {
                if ((x % multiple) == 0) {
                    numberMultiples++;
                }
            }
        }

        return numberMultiples;
    }


    public static void ExFourC() {

        // Função para encontrar múltiplos de 3 e 5 num intervalo de números dado
        // Estrutura de dados
        int minimum, maximum, numberMultiples;
        final int MULTIPLE_ONE = 3, MULTIPLE_TWO = 5;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();

        // Processamento

        numberMultiples = ExFourCGetNumberMultiples(minimum, maximum, MULTIPLE_ONE, MULTIPLE_TWO);

        // Escritura de dados
        System.out.println("Número de múltiplos de " + MULTIPLE_ONE + " e " + MULTIPLE_TWO + " no intervalo dado: " + numberMultiples);
    }

    public static int ExFourCGetNumberMultiples(int minimum, int maximum, int MULTIPLE_ONE, int MULTIPLE_TWO) {
        int numberMultiples = 0;

        for (int x = minimum; x <= maximum; x++) {
            if (x != 0) {
                if ((x % MULTIPLE_ONE) == 0 || (x % MULTIPLE_TWO) == 0) {
                    numberMultiples++;
                }
            }
        }

        return numberMultiples;
    }


    public static void ExFourD() {

        // Função para encontrar múltiplos de dois números dados num intervalo de números dado
        // Estrutura de dados
        int minimum, maximum, numberMultiples, multipleOne, multipleTwo;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();
        System.out.println("Introduza o primeiro número");
        multipleOne = ler.nextInt();
        System.out.println("Introduza o segundo número");
        multipleTwo = ler.nextInt();

        // Processamento

        numberMultiples = ExFourDGetNumberMultiples(minimum, maximum, multipleOne, multipleTwo);

        // Escritura de dados
        System.out.println("Número de múltiplos de " + multipleOne + " e " + multipleTwo + " no intervalo dado: " + numberMultiples);
    }

    public static int ExFourDGetNumberMultiples(int minimum, int maximum, int multipleOne, int multipleTwo) {
        int numberMultiples = 0;

        for (int x = minimum; x <= maximum; x++) {
            if (x != 0) {
                if ((x % multipleOne) == 0 || (x % multipleTwo) == 0) {
                    numberMultiples++;
                }
            }
        }

        return numberMultiples;
    }


    public static void ExFourE() {

        // Função para encontrar a soma dos múltiplos de 2 números dados num intervalo de números dado
        // Estrutura de dados
        int minimum, maximum, sumMultiples, multipleOne, multipleTwo;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();
        System.out.println("Introduza o primeiro número");
        multipleOne = ler.nextInt();
        System.out.println("Introduza o segundo número");
        multipleTwo = ler.nextInt();

        // Processamento

        sumMultiples = ExFourEGetSumMultiples(minimum, maximum, multipleOne, multipleTwo);

        // Escritura de dados
        System.out.println("Número de múltiplos de " + multipleOne + " e " + multipleTwo + " no intervalo dado: " + sumMultiples);
    }

    public static int ExFourEGetSumMultiples(int minimum, int maximum, int multipleOne, int multipleTwo) {
        int sumMultiples = 0;

        for (int x = minimum; x <= maximum; x++) {
            if (x != 0) {
                if ((x % multipleOne) == 0 || (x % multipleTwo) == 0) {
                    sumMultiples = sumMultiples + x;
                }
            }
        }

        return sumMultiples;
    }


    public static void ExFiveA() {

        // Função para calcular a soma de números pares num dado intervalo
        // Estrutura de dados
        int minimum, maximum, sumEvens;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();

        // Processamento

        sumEvens = ExFiveAGetSumEvens(minimum, maximum);

        // Escritura do resultado
        System.out.println("Soma de números pares: " + sumEvens);
    }

    public static int ExFiveAGetSumEvens(int minimum, int maximum) {
        int sumEvens = 0;

        for (int x = minimum; x <= maximum; x++) {
            if ((x % 2) == 0) {
                sumEvens = sumEvens + x;
            }
        }

        return sumEvens;
    }


    public static void ExFiveB() {

        // Função para calcular a quantidade de números pares num dado intervalo
        // Estrutura de dados
        int minimum, maximum, numberEvens;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();

        // Processamento

        numberEvens = ExFiveAGetSumEvens(minimum, maximum);

        // Escritura do resultado
        System.out.println("Quantidade de números pares: " + numberEvens);

    }

    public static int ExFiveBGetNumberEvens(int minimum, int maximum) {
        int numberEvens = 0;

        for (int x = minimum; x <= maximum; x++) {
            if ((x % 2) == 0) {
                numberEvens++;
            }
        }

        return numberEvens;
    }


    public static void ExFiveC() {

        // Função para calcular a soma de números impares num dado intervalo
        // Estrutura de dados
        int minimum, maximum, sumOdds;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();

        // Processamento

        sumOdds = ExFiveCGetSumOdds(minimum, maximum);

        // Escritura do resultado
        System.out.println("Soma de números impares: " + sumOdds);
    }

    public static int ExFiveCGetSumOdds(int minimum, int maximum) {
        int sumOdds = 0;

        for (int x = minimum; x <= maximum; x++) {
            if ((x % 2) != 0) {
                sumOdds = sumOdds + x;
            }
        }

        return sumOdds;
    }


    public static void ExFiveD() {

        // Função para calcular a quantidade de números pares num dado intervalo
        // Estrutura de dados
        int minimum, maximum, numberOdds;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();

        // Processamento

        numberOdds = ExFiveDGetNumberOdds(minimum, maximum);

        // Escritura do resultado
        System.out.println("Quantidade de números impares: " + numberOdds);

    }

    public static int ExFiveDGetNumberOdds(int minimum, int maximum) {
        int numberOdds = 0;

        for (int x = minimum; x <= maximum; x++) {
            if ((x % 2) != 0) {
                numberOdds++;
            }
        }

        return numberOdds;
    }


    public static void ExFiveE() {

        // Função para encontrar a soma dos múltiplos de um número dado num intervalo de números dado que não estejam em ordem
        // Estrutura de dados
        int limitOne, limitTwo, sumMultiples, number;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza um limite do intervalo");
        limitOne = ler.nextInt();
        System.out.println("Introduza outro limite do intervalo");
        limitTwo = ler.nextInt();
        System.out.println("Introduza o número");
        number = ler.nextInt();

        // Processamento

        sumMultiples = ExFiveEGetSumMultiples(limitOne, limitTwo, number);

        // Escritura de dados
        System.out.println("Soma de múltiplos no intervalo dado: " + sumMultiples);
    }

    public static int ReturnSmallerNumber (int numberOne, int numberTwo) {
        int smallerNumber;

        if ( numberOne < numberTwo ) {
            smallerNumber = numberOne;
        } else {
            smallerNumber = numberTwo;
        }

        return smallerNumber;
    }

    public static int ReturnBiggerNumber (int numberOne, int numberTwo) {
        int biggerNumber;

        if ( numberOne > numberTwo ) {
            biggerNumber = numberOne;
        } else {
            biggerNumber = numberTwo;
        }

        return biggerNumber;
    }

    public static int ExFiveEGetSumMultiples(int limitOne, int limitTwo, int number) {
        int sumMultiples = 0;
        int limitLower = ReturnSmallerNumber(limitOne, limitTwo);
        int limitUpper = ReturnBiggerNumber(limitOne, limitTwo);

        for (int x = limitLower; x <= limitUpper; x++) {
            if ((x % number) == 0) {
                sumMultiples = sumMultiples + x;
            }
        }


        return sumMultiples;
    }


    public static void ExFiveF() {

        // Função para encontrar o produto dos múltiplos de um número dado num intervalo de números
        // Estrutura de dados
        int limitOne, limitTwo, productMultiples, number;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        limitOne = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        limitTwo = ler.nextInt();
        System.out.println("Introduza o número");
        number = ler.nextInt();

        // Processamento

        productMultiples = ExFiveFGetProductMultiples(limitOne, limitTwo, number);

        // Escritura de dados
        System.out.println("Produto dos múltiplos no intervalo dado: " + productMultiples);
    }

    public static int ExFiveFGetProductMultiples(int limitOne, int limitTwo, int number) {
        int productMultiples = 1;
        int counter = 0;

        for (int i = limitOne; i <= limitTwo; i++) {
            if (i != 0) {
                if ((i % number) == 0) {
                    productMultiples = productMultiples * i;
                    counter++;
                }
            }
        }

        if (counter == 0) {
            productMultiples = 0;
        }

        return productMultiples;
    }


    public static void ExFiveG() {

        // Função para calcular média dos múltiplos de um número dado num intervalo dado
        // Estrutura de dados
        int limitOne, limitTwo, number;
        double averageMultiples;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza um limite do intervalo");
        limitOne = ler.nextInt();
        System.out.println("Introduza outro limite do intervalo");
        limitTwo = ler.nextInt();
        System.out.println("Introduza o número");
        number = ler.nextInt();

        // Processamento

        averageMultiples = ExFiveGGetAverageMultiples(limitOne, limitTwo, number);

        // Escritura de dados
        System.out.println("Média dos múltiplos no intervalo dado: " + averageMultiples);
    }

    public static double ExFiveGGetAverageMultiples(int limitOne, int limitTwo, int number) {
        int sumMultiples = 0;
        int totalMultiples = 0;

        for (int x = limitOne; x <= limitTwo; x++) {
            if ((x % number) == 0 && x != 0) {
                sumMultiples = sumMultiples + x;
                totalMultiples++;
            }
        }


        return (sumMultiples / (double) totalMultiples);
    }


    public static void ExFiveH() {

        // Função para calcular média dos múltiplos de dois números dados num intervalo dado
        // Estrutura de dados
        int limitOne, limitTwo, numberOne, numberTwo;
        double averageMultiples;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza um limite do intervalo");
        limitOne = ler.nextInt();
        System.out.println("Introduza outro limite do intervalo");
        limitTwo = ler.nextInt();
        System.out.println("Introduza o primeiro número");
        numberOne = ler.nextInt();
        System.out.println("Introduza o segundo número");
        numberTwo = ler.nextInt();

        // Processamento

        averageMultiples = ExFiveHGetAverageMultiples(limitOne, limitTwo, numberOne, numberTwo);

        // Escritura de dados
        System.out.println("Média dos múltiplos no intervalo dado: " + averageMultiples);
    }

    public static double ExFiveHGetAverageMultiples(int limitOne, int limitTwo, int numberOne, int numberTwo) {
        int sumMultiples = 0;
        int totalMultiples = 0;

        for (int x = limitOne; x <= limitTwo; x++) {
            if (((x % numberOne) == 0 || (x % numberTwo == 0)) && x != 0) {
                sumMultiples = sumMultiples + x;
                totalMultiples++;
            }
        }


        return (sumMultiples / (double) totalMultiples);
    }


    public static void ExSixA() {

        // Função para calcular o número de algarismos de um número inteiro longo dado
        // Estrutura de dados
        long number;
        int digits;

        // Leitura de dados

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        number = ler.nextLong();

        // Processamento
        digits = ExSixAGetDigits(number);

        // Escritura de dados
        System.out.println("O número de algarismos é: " + digits);
    }

    public static int ExSixAGetDigits(long number) {
        int digits = 1;

        while (number > 9 || number < -9) {
            digits++;
            number = number / 10;
        }

        return digits;
    }


    public static void ExSixB() {

        // Função para calcular o número de algarismos pares de um número inteiro longo dado
        // Estrutura de dados
        long number;
        int evenDigits;

        // Leitura de dados

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        number = ler.nextLong();

        // Processamento
        evenDigits = ExSixBGetNumberEvenDigits(number);

        // Escritura de dados
        System.out.println("O número de algarismos pares é: " + evenDigits);
    }

    public static int ExSixBGetNumberEvenDigits(long number) {
        int evenDigits = 0;

        while (number != 0) {

            if (number % 2 == 0) {
                evenDigits++;
            }

            number = number / 10;
        }

        return evenDigits;
    }


    public static void ExSixC() {

        // Função para calcular o número de algarismos impares de um número inteiro longo dado
        // Estrutura de dados
        long number;
        int oddDigits;

        // Leitura de dados

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        number = ler.nextLong();

        // Processamento
        oddDigits = ExSixCGetNumberOddDigits(number);

        // Escritura de dados
        System.out.println("O número de algarismos impares é: " + oddDigits);
    }

    public static int ExSixCGetNumberOddDigits(long number) {
        int oddDigits = 0;

        while (number != 0) {

            if (number % 2 != 0) {
                oddDigits++;
            }

            number = number / 10;
        }

        return oddDigits;
    }


    public static void ExSixD() {

        // Função para calcular a soma dos algarismos de um número inteiro longo dado
        // Estrutura de dados
        long number;
        double digits;

        // Leitura de dados

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        number = ler.nextLong();

        // Processamento
        digits = ExSixDGetSumDigits(number);

        // Escritura de dados
        System.out.println("O soma dos algarismos é: " + String.format("%.2f", digits));
    }

    public static double ExSixDGetSumDigits(long number) {
        double sumDigits = 0;

        while (number != 0) {
            sumDigits = sumDigits + (number % 10);
            number = number / 10;
        }

        return sumDigits;
    }


    public static void ExSixE() {

        // Função para calcular a soma dos algarismos pares de um número inteiro longo dado
        // Estrutura de dados
        long number;
        double digits;

        // Leitura de dados

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        number = ler.nextLong();

        // Processamento
        digits = ExSixEGetSumEvenDigits(number);

        // Escritura de dados
        System.out.println("O número de algarismos pares é: " + String.format("%.2f", digits));
    }

    public static double ExSixEGetSumEvenDigits(long number) {
        double sumDigits = 0;

        while (number != 0) {
            if (number % 2 == 0) {
                sumDigits = sumDigits + (number % 10);
            }
            number = number / 10;
        }

        return sumDigits;
    }


    public static void ExSixF() {

        // Função para calcular a soma dos algarismos impares de um número inteiro longo dado
        // Estrutura de dados
        long number;
        double digits;

        // Leitura de dados

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        number = ler.nextLong();

        // Processamento
        digits = ExSixFGetSumOddDigits(number);

        // Escritura de dados
        System.out.println("O número de algarismos impares é: " + String.format("%.2f", digits));
    }

    public static double ExSixFGetSumOddDigits(long number) {
        double sumDigits = 0;

        while (number != 0) {
            if (number % 2 != 0) {
                sumDigits = sumDigits + (number % 10);
            }
            number = number / 10;
        }

        return sumDigits;
    }


    public static void ExSixG() {

        // Função para calcular a média dos algarismos de um número inteiro longo dado
        // Estrutura de dados
        long number;
        double averageDigits;

        // Leitura de dados

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número");
        number = ler.nextLong();

        // Processamento
        averageDigits = ExSixGGetAverageDigits(number);

        // Escritura de dados
        System.out.println("O média dos algarismos é: " + String.format("%.2f", averageDigits));
    }

    public static double ExSixGGetAverageDigits(long number) {
        double sumDigits = 0;
        double averageDigits;
        int i = 0;

        while (number != 0) {
            sumDigits = sumDigits + (number % 10);
            number = number / 10;
            i++;
        }
        averageDigits = sumDigits / (double) i;

        return averageDigits;
    }


    public static double ExSixHGetAverageEvenDigits(long number) {
        double sumDigits = 0;
        double averageEvenDigits;
        int i = 0;

        while (number != 0) {
            if (number % 2 == 0) {
                sumDigits = sumDigits + (number % 10);
                i++;
            }
            number = number / 10;
        }
        averageEvenDigits = sumDigits / (double) i;

        return averageEvenDigits;
    }


    public static double ExSixIGetAverageOddDigits(long number) {
        double sumDigits = 0;
        double averageOddDigits;
        int i = 0;

        while (number != 0) {
            if (number % 2 != 0) {
                sumDigits = sumDigits + (number % 10);
                i++;
            }
            number = number / 10;
        }
        averageOddDigits = sumDigits / (double) i;

        return averageOddDigits;
    }


    public static long ExSixJReverseDigits(long number) {
        long reversedNumber = 0;

        while (number != 0) {
            reversedNumber = reversedNumber * 10 + number % 10;
            number = number / 10;
        }

        return reversedNumber;

    }


    public static boolean ExSevenACheckIfNumeralPalindrome(int number) {
        boolean isNumeralPalindrome = false;
        int reversedNumber = 0;
        int reversing = number;

        while (reversing != 0) {
            reversedNumber = reversedNumber * 10 + reversing % 10;
            reversing = reversing / 10;
        }

        if (reversedNumber == number) {
            isNumeralPalindrome = true;
        }

        return isNumeralPalindrome;
    }


    public static boolean ExSevenBCheckIfArmstrongNumber(int number) {
        boolean isArmstrongNumber = false;
        double armstrongNumber = 0;
        int temporary = number;

        while (temporary != 0) {
            armstrongNumber = armstrongNumber + Math.pow(temporary % 10, 3);
            temporary = temporary / 10;
        }

        if (armstrongNumber == number) {
            isArmstrongNumber = true;
        }

        return isArmstrongNumber;
    }


    public static void ExSevenC() {

        // Função para encontrar a primeira capicua num intervalo dado
        // Estrutura de dados
        int minimum, maximum, firstPalindrome;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o limite mínimo do intervalo");
        minimum = ler.nextInt();
        System.out.println("Introduza o limite máximo do intervalo");
        maximum = ler.nextInt();

        // Processamento
        firstPalindrome = ExSevenCCheckIntervalNumbersFirstPalindrome(minimum, maximum);

        // Escritura de dados
        System.out.println("O primeiro número capicua é :" + firstPalindrome);
    }

    public static int ExSevenCCheckIntervalNumbersFirstPalindrome(int minimum, int maximum) {
        int firstPalindrome = -1;

        for (int i = minimum; i <= maximum; i++) {
            if (ExSevenACheckIfNumeralPalindrome(i) && firstPalindrome == -1) {
                firstPalindrome = i;
            }
        }

        return firstPalindrome;
    }


    public static int ExSevenDCheckIntervalNumbersLastPalindrome(int minimum, int maximum) {
        int firstPalindrome = -1;

        for (int i = minimum; i <= maximum; i++) {
            boolean isPalindrome = ExSevenACheckIfNumeralPalindrome(i);
            if (isPalindrome) {
                firstPalindrome = i;
            }
        }

        return firstPalindrome;
    }


    public static int ExSevenECheckIntervalNumbersCountPalindrome(int minimum, int maximum) {
        int countPalindrome = 0;

        for (int i = minimum; i <= maximum; i++) {
            boolean isPalindrome = ExSevenACheckIfNumeralPalindrome(i);
            if (isPalindrome) {
                countPalindrome++;
            }
        }

        return countPalindrome;
    }


    public static int ExSevenFCheckIntervalNumbersFirstArmstrong(int minimum, int maximum) {
        int firstArmstrong = -1;

        for (int i = minimum; i <= maximum; i++) {
            boolean isArmstrongNumber = ExSevenBCheckIfArmstrongNumber(i);
            if (isArmstrongNumber) {
                firstArmstrong = i;
                break;
            }
        }

        return firstArmstrong;
    }


    public static int ExSevenGCheckIntervalNumbersCountArmstrong(int minimum, int maximum) {
        int countArmstrong = 0;

        for (int i = minimum; i <= maximum; i++) {
            boolean isArmstrongNumber = ExSevenBCheckIfArmstrongNumber(i);
            if (isArmstrongNumber) {
                countArmstrong++;
            }
        }

        return countArmstrong;
    }


    public static void ExEight() {

        // Ler números até que a soma destes seja superior a um dado número, imprimir o menor
        // Estrutura de dados
        double maximum, number, sum, smallest;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o máximo");
        maximum = ler.nextDouble();

        System.out.println("Introduza um número");
        number = ler.nextDouble();
        sum = number;
        smallest = number;

        while (sum < maximum) {
            System.out.println("Introduza um número");
            number = ler.nextDouble();

            if (number < smallest) {
                smallest = number;
            }

            sum += number;
        }

        System.out.println(smallest);
    }


    public static void ExNine() {

        double totalSalary = 0, averageSalary;
        double[] baseSalary = new double[1];
        double[] overtimeHours = new double[1];

        int i = 0;
        double check;
        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Introduza as horas extra do funcionário");
            check = ler.nextDouble();
            if (check != -1) {
                if (i == 0) {
                    overtimeHours[i] = ler.nextDouble();
                } else {
                    overtimeHours = AddValueToArray(overtimeHours, ler.nextDouble());
                }

                System.out.println("Introduza o salário base do funcionário");
                if (i == 0) {
                    baseSalary[i] = ler.nextDouble();
                } else {
                    baseSalary = AddValueToArray(baseSalary, ler.nextDouble());
                }
                i++;
            }

        } while (check != -1);

        double[] salarioMensal = new double[overtimeHours.length];

        for (i = 0; i < overtimeHours.length; i++) {
            salarioMensal[i] = baseSalary[i] + overtimeHours[i] * (baseSalary[i] * 0.02);

            totalSalary += salarioMensal[i];

            System.out.print(salarioMensal[i] + "; ");
        }

        averageSalary = totalSalary / salarioMensal.length;

        System.out.println(averageSalary);

    }


    public static double[] AddValueToArray(double[] array, double value) {

        int lenghtNewArray = array.length + 1;
        double[] newArray = new double[lenghtNewArray];

        for (int i = 0; i < lenghtNewArray - 1; i++) {
            newArray[i] = array[i];
        }

        newArray[lenghtNewArray - 1] = value;

        return newArray;
    }


    public static void ExTen() {

        // Ler números até que a soma destes seja superior a um dado número, imprimir o maior
        // Estrutura de dados
        double maximum, number, sum, largest;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o máximo");
        maximum = ler.nextDouble();

        System.out.println("Introduza um número");
        number = ler.nextDouble();
        sum = number;
        largest = number;

        while (sum < maximum) {
            System.out.println("Introduza um número");
            number = ler.nextDouble();

            if (number > largest) {
                largest = number;
            }

            sum += number;
        }

        System.out.println(largest);
    }


    public static void ExEleven() {

        // Função para apresentar quantas somas com números de 0 a 10 para chegar a um número dado de 0 a 20
        // Estrutura de dados
        int number, counter = 0;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de 0 a 20");
        number = ler.nextInt();

        while (number < 0 || number > 20) {
            System.out.println("Número inválido. Introduza o número de 0 a 20");
            number = ler.nextInt();
        }

        for (int i = 0; i <= 10; i++) {
            int j = i;
            while ( j <= number ) {
                if (i + j == number) {
                    System.out.println(i + "+" + j);
                    counter++;
                    j = number + 1;
                }
                j++;

            }
        }
        System.out.println(counter + " somas");
    }


    public static void ExTwelve() {

        // Determinar raízes de N equações de segundo grau
        // Estrutura de dados
        double a, b, c;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza A");
        a = ler.nextDouble();
        System.out.println("Introduza B");
        b = ler.nextDouble();
        System.out.println("Introduza C");
        c = ler.nextDouble();

        // Processamento
        if (a == 0) {
            System.out.println("Equação não é de segundo grau");
        }

        double check = Math.pow(b, 2) - 4 * a * c;

        if (check > 0) {
            System.out.println("Equação tem 2 raízes reais");
            System.out.println("X1: " + (-b + Math.sqrt(check)) / 2 * a);
            System.out.println("X2: " + (-b - Math.sqrt(check)) / 2 * a);
        }

        if (check == 0) {
            System.out.println("Equação tem raíz dupla");
            System.out.println("X: " + (-b + Math.sqrt(check)) / 2 * a);
        }

        if (check < 0) {
            System.out.println("Equação tem raízes imaginárias");
        }
    }


    public static void ExThirteen() {

        // Recebe códigos e classifica-os.
        // Estrutura de dados
        int code;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Introduza um código");
            code = ler.nextInt();
            if (code < 0 || code > 15) {
                System.out.println("Código Inválido");
            } else if (code == 1) {
                System.out.println("Alimento não perecível");
            } else if (code >= 2 && code <= 4) {
                System.out.println("Alimento perecível");
            } else if (code == 5 || code == 6) {
                System.out.println("Vestuário");
            } else if (code == 7) {
                System.out.println("Higiene pessoal");
            } else if (code >= 8) {
                System.out.println("Limpeza e utensílios domésticos");
            }
        } while (code != 0);

    }


    public static void ExFourteen() {

        // Função para câmbio de euros
        // Estrutura de dados
        double value, currency;

        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Introduza o valor");
            value = ler.nextDouble();

            if (value >= 0) {
                System.out.println("Escolha a moeda de câmbio \n 1 - Dólar \n 2 - Libra \n 3 - Iene \n 4 - Coroa Sueca \n 5 - Franco Suíço");
                currency = ler.nextDouble();
                if (currency == 1) {
                    System.out.println(value * 1.534);
                } else if (currency == 2) {
                    System.out.println(value * 0.774);
                } else if (currency == 3) {
                    System.out.println(value * 161.480);
                } else if (currency == 4) {
                    System.out.println(value * 9.593);
                } else if (currency == 5) {
                    System.out.println(value * 1.601);
                } else {
                    System.out.println("Inválido");
                }
            }
        } while (value >= 0);
    }


    public static void ExFifteen() {

        // Função para atribuir classificação a notas
        // Estrutura de dados
        int grade;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        do {
            System.out.println("Introduza uma nota");
            grade = ler.nextInt();
            if (grade >= 0 && grade <= 4) {
                System.out.println("Mau");
            } else if (grade >= 5 && grade <= 9) {
                System.out.println("Medíocre");
            } else if (grade >= 10 && grade <= 13) {
                System.out.println("Suficiente");
            } else if (grade >= 14 && grade <= 17) {
                System.out.println("Bom");
            } else if (grade >= 18 && grade <= 20) {
                System.out.println("Muito Bom");
            } else if (grade > 20) {
                System.out.println("Nota inválida");
            }
        } while (grade > 0);
    }


    public static void ExSixteen() {

        // Calcular salário líquido através de salário bruto e imposto gradual
        // Estrutura de dados
        double salarioBruto, salarioLiquido;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o salário bruto");
        salarioBruto = ler.nextDouble();

        // Processamento

        if ( salarioBruto > 1000 ) {
            salarioLiquido = (salarioBruto - 1000) * 0.8 + 500 * 0.85 + 500 * 0.9;
            System.out.println(salarioLiquido);
        } else if ( salarioBruto <= 1000 && salarioBruto > 500 ) {
            salarioLiquido = (salarioBruto - 500) * 0.85 + 500 * 0.9;
            System.out.println(salarioLiquido);
        } else if ( salarioBruto <= 500 && salarioBruto > 0 ) {
            salarioLiquido = salarioBruto * 0.9;
            System.out.println(salarioLiquido);
        } else {
            System.out.println("Valor inválido");
        }
    }


    public static void ExSeventeen() {

        // Função para classificar peso do cão e quantidade de ração
        // Estrutura de dados
        double dogWeight=0, dogFood;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        while (dogWeight>=0) {

            System.out.println("Introduza o peso do cão");
            dogWeight = ler.nextDouble();
            System.out.println("Introduza a quantidade de ração");
            dogFood = ler.nextDouble();

            if (dogWeight > 0 && dogWeight <= 10) {
                if (dogFood == 100) {
                    System.out.println("Quantidade adequada para cão pequeno");
                } else {
                    System.out.println("NÃO, SÃO 100 GRAMAS");
                }
            }

            if (dogWeight > 10 && dogWeight <= 25) {
                if (dogFood == 250) {
                    System.out.println("Quantidade adequada para cão médio");
                } else {
                    System.out.println("NÃO, SÃO 250 GRAMAS");
                }
            }

            if (dogWeight > 25 && dogWeight <= 45) {
                if (dogFood == 300) {
                    System.out.println("Quantidade adequada para cão grande");
                } else {
                    System.out.println("NÃO, SÃO 300 GRAMAS");
                }
            }

            if (dogWeight > 45) {
                if (dogFood == 500) {
                    System.out.println("Quantidade adequada para cão gigante");
                } else {
                    System.out.println("NÃO, SÃO 500 GRAMAS");
                }
            }

        }
    }


    public static void ExEighteen() {

        // Função para validar o número de um cartão de cidadão
        // Estrutura de dados
        int idNumber, idWeightedSum, digit;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de identificação");
        idNumber = ler.nextInt();

        // Processamento

        idWeightedSum = GetWeightedSum(idNumber);

        System.out.println(IsValidIdNumber(idWeightedSum));
    }

    public static int GetWeightedSum(int number) {
        int i = 1;
        int weightedSum = 0;
        int digit;

        while (number != 0) {
            digit = number % 10;
            weightedSum += (digit * i);
            number /= 10;
            i++;
        }

        return weightedSum;
    }

    public static boolean IsValidIdNumber(int weightedSum) {
        boolean validIdNumber = false;

        if ((weightedSum%11) == 0) {
            validIdNumber = true;
        }

        return validIdNumber;
    }

    public static int GetEvensOnly(int number) {
        int digit;
        int evensOnly=0;

        while (number > 0) {
            digit = number % 10;
            number /= 10;
            if (isEven(digit)) {
                evensOnly = evensOnly*10 + digit;
            }
        }

        return evensOnly;
    }


    public static int GetOddsOnly(int number) {
        int digit;
        int oddsOnly=0;

        while (number > 0) {
            digit = number % 10;
            if (!isEven(digit)) {
                oddsOnly = oddsOnly*10 + digit;
            }
            number /= 10;
        }

        return oddsOnly;
    }

    public static boolean isEven(int number) {
        boolean isEven = false;

        if ((number % 2) == 0) {
            isEven = true;
        }

        return isEven;
    }

    public static int ExNineteen(int number) {
        int sortedOddsEvens;
        int digit;

        sortedOddsEvens = GetOddsOnly(number);
        while (number > 0) {
            digit = number % 10;
            number /= 10;

            if (isEven(digit)) {
                sortedOddsEvens = sortedOddsEvens * 10 + digit;
            }
        }

        return sortedOddsEvens;
    }


    public static void ExTwenty() {

        // Classificar um número como perfeito, abundante ou reduzido
        // Estrutura de dados
        int sumDivisors, number;

        // Leitura de dados
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza um número");
        number = ler.nextInt();

        // Processamento
        sumDivisors = GetSumDivisors(number);

        if (sumDivisors==number){
            System.out.println("Número Perfeito");
        } else if (sumDivisors<number){
            System.out.println("Número reduzido");
        } else {
            System.out.println("Número abundante");
        }

    }

    public static int GetSumDivisors(int number) {
        int sumDivisors = 0;

        for (int i=1; i<number; i++){
            if(IsDivisor(i, number)){
                sumDivisors += i;
            }
        }

        return sumDivisors;
    }

    public static boolean IsDivisor(int potentialDivisor, int number) {
        boolean isDivisor = false;

        if ( (number % potentialDivisor) == 0) {
            isDivisor = true;
        }

        return isDivisor;
    }

}